import { computed, makeObservable, observable } from 'mobx';
import { DateTime, Interval } from 'luxon';
import type { Point } from 'geojson';

export type ContactTypes = 'wa' | 'tg' | 'fb' | 'vk' | 'gl' | 'gh' | 'phone' | 'email';

export type Contacts = Partial<Record<ContactTypes, string>>;

export interface CommonInfo {
  name: string;
  birthDay: Date;
  age: string;
  email: string;
  location: string;
}

export interface ExperienceItem {
  company?: string;
  site?: string | string[];
  position?: string;
  categories: string[];
  description: string;
  tags: string[];
  location?: Point | Point[];
  since: Date;
  until?: Date;
}

export interface Experience {
  readonly tags: readonly string[];
  jobs: ExperienceItem[];
  readonly detailedStage: Record<string, number>;
  readonly stage: number;
  readonly workPeriod: [Date, Date];
}

export class CvState implements CommonInfo, Experience {
  @observable public lngLoaded: boolean = false;
  @observable public lng?: string;
  @observable public contacts: Contacts = {
    wa: 'https://wa.me/qr/BJS5CQIZ5C3EM1',
    tg: 'https://t.me/Saymon91',
    fb: 'https://www.facebook.com/Simon.A.Zhuravlev',
    vk: 'https://vk.com/saymon91',
    gl: 'https://gitlab.com/Saymon91',
    gh: 'https://github.com/Saymon91',
  };
  @observable public name: string = 'cv:main_info.value.full_name';
  @observable public birthDay = new Date('1991-01-24T00:00:00.000Z');
  @computed public get age(): string {
    return Interval.fromDateTimes(this.birthDay, new Date()).length('years').toFixed(0);
  }
  @computed public get email(): string {
    return this.contacts.email ?? '-';
  }
  @observable public location = 'cv:main_info.value.location';

  @observable public jobs: ExperienceItem[] = [
    {
      company: 'buzdp1.company',
      position: 'buzdp1.position',
      site: 'https://ordpol1.ru',
      categories: ['development'],
      description: 'buzdp1.description',
      since: new Date('2013-09-01'),
      until: new Date('2015-08-31'),
      tags: ['php', 'javascript', 'mysql'],
      location: [
        { type: 'Point', coordinates: [52.956865, 36.061008] },
        { type: 'Point', coordinates: [52.950718, 36.025919] },
      ],
    },
    {
      company: 'ntcnvg.company',
      position: 'ntcnvg.position',
      site: ['https://nvg-group.ru', 'http://www.mongeo.ru'],
      categories: ['development'],
      description: 'ntcnvg.description',
      since: new Date('2015-10-01'),
      until: new Date('2016-05-31'),
      tags: ['javascript', 'node.js', 'mongodb', 'redis', 'rabbitmq', 'microservices', 'docker'],
      location: [
        { type: 'Point', coordinates: [52.977849, 36.100857] },
      ],
    },
    {
      company: 'komnet.company',
      position: 'komnet.position1',
      site: 'https://rightech.io',
      categories: ['development'],
      description: 'komnet.description1',
      since: new Date('2016-06-01'),
      until: new Date('2017-06-31'),
      tags: ['javascript', 'node.js', 'mongodb', 'redis', 'rabbitmq', 'microservices', 'php', 'yi2', 'mysql', 'kubernetes'],
      location: [
        { type: 'Point', coordinates: [55.784587, 37.672118] },
      ],
    },
    {
      company: 'komnet.company',
      position: 'komnet.position2',
      site: 'https://rightech.io',
      categories: ['development'],
      description: 'komnet.description1',
      since: new Date('2017-07-01'),
      until: new Date('2018-11-02'),
      tags: ['javascript', 'node.js', 'mongodb', 'redis', 'rabbitmq', 'microservices', 'php', 'yi2', 'mysql', 'scala', 'akka'],
      location: [
        { type: 'Point', coordinates: [55.784587, 37.672118] },
      ],
    },
    {
      company: 'delimobil.company',
      position: 'delimobil.position',
      site: 'https://delimobil.ru',
      categories: ['development'],
      description: 'delimobil.description',
      since: new Date('2018-12-01'),
      until: new Date('2019-06-12'),
      tags: ['javascript', 'node.js', 'redis', 'rabbitmq', 'microservices', 'mysql', 'typescript', 'moleculer.js', 'docker'],
      location: [
        { type: 'Point', coordinates: [55.784587, 37.672118] },
      ],
    },
    {
      company: 'profit.company',
      position: 'profit.position',
      site: ['https://prof-it.ru', 'https://parkovkinn.ru/'],
      categories: ['management', 'development'],
      description: 'profit.description',
      since: new Date('2019-08-01'),
      until: new Date('2021-05-31'),
      tags: ['typescript', 'node.js', 'nest.js', 'redis', 'postgresql', 'postgis', 'docker'],
      location: [
        { type: 'Point', coordinates: [55.706573, 37.597091] },
        { type: 'Point', coordinates: [56.327610, 44.004217] },
      ],
    },
    {
      company: 'x5.company',
      position: 'x5.position',
      site: 'https://dostavka.okolo.app/',
      categories: ['management', 'development'],
      description: 'x5.description',
      since: new Date('2021-06-07'),
      tags: ['typescript', 'node.js', 'redis', 'mongodb'],
      location: [
        { type: 'Point', coordinates: [55.706573, 37.597091] },
        { type: 'Point', coordinates: [56.327610, 44.004217] },
      ],
    },
  ];
  @computed public get tags(): readonly string[] {
    return [...new Set(this.jobs.reduce<string[]>(
      (acc, { tags }) => tags?.length ? [...acc, ...tags] : acc,
      [],
    ))];
  }
  @computed public get detailedStage(): Record<string, number> {
    const now = new Date();
    const stage: Record<string, number> = {};
    for (const { categories, since, until = now } of this.jobs) {
      const duration = Interval.fromDateTimes(since, until).length('years');
      for (const cat of categories) {
        stage[cat] = (stage[cat] ?? 0) + duration;
      }
    }
    return stage;
  }
  @computed public get stage(): number {
    const now = new Date();
    return this.jobs.reduce(
      (acc, { since, until = now }) => acc + Interval.fromDateTimes(since, until).length('years'),
      0,
    );
  }
  @computed public get workPeriod(): [Date, Date] {
    const now = new Date();
    const [sinceArr, untilArr] = this.jobs.reduce<[Date[], Date[]]>(
      ([sinceArr, untilArr], { since, until = now }) => {
        sinceArr.push(since);
        untilArr.push(until);
        return [sinceArr, untilArr];
      },
      [[], []],
    );

    return [
      new Date(Math.min(...sinceArr.map(v => +v))),
      new Date(Math.max(...untilArr.map(v =>+v))),
    ];
  }

  @computed public get isLoaded(): boolean {
    return this.lngLoaded;
  }
  constructor() {
    makeObservable(this);
  }
}
