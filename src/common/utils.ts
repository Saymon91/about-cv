import type { DateInput } from 'luxon';
import { DateTime } from 'luxon';

export const toDateTime = (value: DateInput | number | string): DateTime =>
  typeof value === 'string'
    ? DateTime.fromISO(value)
    : typeof value === 'number'
      ? DateTime.fromMillis(value)
      : value instanceof Date
        ? DateTime.fromJSDate(value)
        : value instanceof DateTime
          ? value
          : DateTime.fromObject(value);
