import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { observer } from 'mobx-react-lite';

import Main from './pages/main'
import InProgress from './pages/in-progress';
import Experience from './pages/experience';
import ExperienceItem from './pages/experience-item';
import type { CvState } from './mobx';

export interface Props {
  state: CvState;
}

export default observer<Props>(({ state }) => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path='/' element={<Main state={state} />}>
          <Route path='experience'>
            <Route index element={<Experience state={state} />} />
            <Route path=':company/:since' element={<ExperienceItem state={state} />} />
            <Route path=':company/:since/:until' element={<ExperienceItem state={state} />} />
          </Route>
          <Route path='*' element={<InProgress />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
});
