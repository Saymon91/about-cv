import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './app';
import { CvState } from './mobx';

const state = new CvState();

test('renders learn react link', () => {
  render(<App state={state} />);
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});
