import React from 'react';
import { useLocation } from 'react-router-dom';
import { Container } from 'react-bootstrap';

export const Component = () => {
  const location = useLocation();

  return (<Container>
    {location.pathname} in progress
  </Container>);
};

export default Component;
