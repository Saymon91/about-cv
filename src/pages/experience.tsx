import React, { useEffect, useRef, useState } from 'react';
import { observer } from 'mobx-react-lite';
import { Container, Table } from 'react-bootstrap';
import { DateTime, Interval } from 'luxon';
import { useTranslation } from 'react-i18next';

import type { CvState } from '../mobx';

import WorkTimeline from '../components/work-timeline';

export interface Props {
  state: CvState;
}

export const Component = observer<Props>(({ state }) => {
  const [t, i18n] = useTranslation();
  const [wpSince, wpUntil] = state.workPeriod;
  
  const workPeriod = Interval.fromDateTimes(
    wpSince,
    wpUntil,
  );
  const workPeriodLength = workPeriod.length('years');

  return (<Container fluid>
    <Table>
      <tbody>
      <tr>
        <td>{t('cv:experience.label.full_stage')}</td>
        <td>{state.stage.toFixed(1)}</td>
      </tr>
      <tr>
        <td colSpan={2} className='text-center'>{t('cv:experience.label.detailed_stage')}</td>
      </tr>
      {Object.entries(state.detailedStage).map(([cat, stage], index) => (<tr key={index}>
        <td>{t(`cv:experience.label.cat_${cat}`)}</td>
        <td>{stage.toFixed(1)}</td>
      </tr>))}
      <tr>
        <td colSpan={2} className='text-center'>{t('cv:experience.label.work_period')}</td>
      </tr>
      <tr>
        <td className='text-center'>{DateTime.fromJSDate(wpSince).toLocaleString({}, { locale: i18n.language })}</td>
        <td className='text-center'>{DateTime.fromJSDate(wpUntil).toLocaleString({}, { locale: i18n.language })}</td>
      </tr>
      <tr>
        <td colSpan={2}>{workPeriodLength.toFixed(1)}</td>
      </tr>
      </tbody>
    </Table>
    <WorkTimeline
      tags={state.tags}
      jobs={state.jobs}
      detailedStage={state.detailedStage}
      stage={state.stage}
      workPeriod={state.workPeriod}
    />
  </Container>);
});

export default Component;
