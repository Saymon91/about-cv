import React from 'react';
import { observer } from 'mobx-react-lite';
import { Container } from 'react-bootstrap';
import { useLocation, Outlet } from 'react-router-dom';
import type { Location } from 'react-router-dom';
import styled from 'styled-components';

import MainInfo from '../components/main-info';
import MainMenu from '../components/main-menu';
import type { CvState } from '../mobx';

export interface Props {
  state: CvState;
}

const MainInfoWrapperView = styled.div<{ location: Location; }>`
  width: ${props => props.location.pathname === '/' ? '50vw' : '0' };
  z-index: 1;
  transition-duration: 200ms;
`;

const MainMenuWrapperView = styled.div<{ location: Location; }>`
  min-width: 220px;
  width: ${props => props.location.pathname === '/' ? '50vw' : '25vw' };
  box-shadow: ${props => props.location.pathname === '/' ? 'none' : '0 0 10px rgba(0, 0, 0, 10%)'};
  z-index: 1;
  transition-duration: 200ms;
`;

const MainContentWrapperView = styled.div<{ location: Location; }>`
  width: ${props => props.location.pathname === '/' ? '0' : '75vw' };
  background-color: var(--bs-grey);
  transition-duration: 200ms;
  padding-top: ${props => props.location.pathname === '/' ? '0' : '5rem'};
  padding-left: ${props => props.location.pathname === '/' ? '0' : '1rem'};
`;

export const Component = observer<Props>(({ state }) => {
  const location = useLocation();

  return (
    <Container fluid className="p-0 m-0 h-100 d-flex flex-row overflow-hidden">
      <MainInfoWrapperView location={location} className="overflow-hidden p-0 h-100">
        <MainInfo state={state}/>
      </MainInfoWrapperView>
      <MainMenuWrapperView location={location} className="overflow-hidden p-0 h-100">
        <MainMenu state={state}/>
      </MainMenuWrapperView>
      <MainContentWrapperView location={location} className='d-block position-relative overflow-scroll'>
        <Container className='bg-white p-4 d-block position-relative' style={{ minHeight: '100%', height: 'auto', maxWidth: '1024px', boxShadow: '0 0 10px rgba(0, 0, 0, 10%)' }}>
          <Outlet />
        </Container>
      </MainContentWrapperView>
    </Container>
  );
});

export default Component;
