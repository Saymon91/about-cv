import React from 'react';
import { observer } from 'mobx-react-lite';
import { useLocation, useNavigate, useParams } from 'react-router-dom';
import { Container } from 'react-bootstrap';

import type { CvState } from '../mobx';
import { useTranslation } from 'react-i18next';
import { DateTime } from 'luxon';

export interface Props {
  state: CvState;
}

interface Params {
  company: string;
  since: string;
  until?: string;
}

export const Component = observer<Props>(({ state }) => {
  const [t] = useTranslation();
  const location = useLocation();
  const navigate = useNavigate();
  const { company, since, until } = useParams();

  const job = state.jobs.find((job) => {
    return job.company === company && job.since >= new Date(since!) && (!until && !job.until || until && job.until! <= DateTime.fromJSDate(new Date(until)).endOf('month').toJSDate());
  });

  return (<Container fluid>
    {job ? JSON.stringify(job) : t('cv:experience.details.not_found', { company, since, until })}
  </Container>);
});

export default Component;
