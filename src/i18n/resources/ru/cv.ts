export const nav = {
  link: {
    home: 'главная',
    about: 'обо мне',
    experience: 'мой опыт',
    portfolio: 'выполненные проекты',
    education: 'мое образование',
    contacts: 'как связаться',
  }
}

export const main_info = {
  label: {
    name: 'имя',
    age: 'возраст',
    birth_day: 'день рождения',
    email: 'e-mail',
    location: 'адрес',
    phone: 'моб.номер',
  },
  value: {
    full_name: 'Семен Журавлев',
    location: 'Россия, Москва'
  }
}

export const typewriter = {
  row: {
    name: 'Меня зовут <span class=\'text-primary fw-bold\'>Семен</span>',
    job: 'Я управляю <span class=\'text-primary fw-bold\'>разработкой ПО</span>',
    location: 'Я живу в <span class=\'text-primary fw-bold\'>Москве</span>',
  }
}

export const duration = {
  month_short: '{{months}} мес.',
  year_short_interval: '(0)[л.];(1-4)[г.];(5-20)[л.];',
  month_interval: '(1)[месяц];(2-4)[месяца];(5-11)[месяцев]',
  year_interval: '(0)[{{years}} лет];(1)[{{years}} год];(2-4)[{{years}} года];(5-20)[{{years}} лет]',
  year_float: '{{years}} года',
  until_now: 'по н.вр.',
}

export const experience = {
  full_duration: '$t(cv:duration.year_interval, { "postProcess": "interval", "count": {{years}} }) $t(cv:duration.month_interval, { "postProcessor": "interval", "count": {{months}} })',
  x5: {
    position: 'Руководитель группы разработки',
    company: 'X5 digital',
    description: `Разработка системы управления курьерами прочей лабудой. Разработка системы управления курьерами прочей лабудой.
    Разработка системы управления курьерами прочей лабудой. Разработка системы управления курьерами прочей лабудой.
    Разработка системы управления курьерами прочей лабудой. Разработка системы управления курьерами прочей лабудой.
    <span class='text-primary'>ААА</span>
    `
  },
  profit: {

  }
}
