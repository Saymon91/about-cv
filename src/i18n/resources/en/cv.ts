export const full_name = 'Simon Zhuravlev';

export const nav = {
  link: {
    home: 'home',
    about: 'about',
    experience: 'experience',
    portfolio: 'done projects',
    education: 'education',
    contacts: 'contacts',
  }
};
