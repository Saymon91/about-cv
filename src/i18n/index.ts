import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import intervalPlural from 'i18next-intervalplural-postprocessor';

import type { CvState } from '../mobx';

import * as resources from './resources';

export default i18n
  .use(LanguageDetector)
  .use(initReactI18next)
  .use(intervalPlural);

export const init = (state: CvState) => {
  state.lngLoaded = false;
  i18n.init({
    fallbackLng: 'en',
    debug: true,
    load: 'languageOnly',
    cleanCode: true,
    saveMissing: true,
    contextSeparator: ':',
    resources,

    supportedLngs: ['en', 'ru'],
    ns: ['cv'],

    interpolation: {
      escapeValue: false, // not needed for react as it escapes by default
    },
  }).then(
    () => {
      state.lng = i18n.language;
      state.lngLoaded = true;
    },
    err => {
      console.error(err);
    },
  )
}
