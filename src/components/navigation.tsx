import React, { PropsWithChildren } from 'react';
import { Link, useLocation } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { Container } from 'react-bootstrap';

const menuItems: [string, string][] = [
  ['home', '/'],
  ['experience', '/experience'],
  ['education', '/education'],
  ['portfolio', '/portfolio'],
  ['contacts', '/contacts'],
]

interface MenuItemLinkProps {
  link: string;
  title: string;
}

const MenuItem = ({ className = '', children, style = {}, ...props }: React.DetailedHTMLProps<React.HTMLAttributes<HTMLParagraphElement>, HTMLParagraphElement>) => {
  const classes = className.split(' ');
  classes.push('text-uppercase', 'mb-4');
  return (<p {...props} style={{ ...style, fontSize: '0.9em', fontWeight: 400 }} className={classes.join(' ')}>{children}</p>);
}

const MenuLinkItem = ({ title, link }: PropsWithChildren<MenuItemLinkProps>) => (
  <Link to={link} style={{ textDecoration: 'none' }}>
    <MenuItem className='text-dark'>{title}</MenuItem>
  </Link>
);

export const Component = () => {
  const location = useLocation();
  const [t] = useTranslation();

  return (<Container as='nav' className='d-flex flex-column justify-content-center align-items-center'>
    {menuItems.map(([title, link], index) => (link === location.pathname
      ? <MenuItem className='text-primary' key={index}>{t(`cv:nav.link.${title}`)}</MenuItem>
      : <MenuLinkItem title={t(`cv:nav.link.${title}`)} link={link} key={index}/>))}
  </Container>);
}

export default Component;
