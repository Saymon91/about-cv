import React, { createContext, useEffect, useState } from 'react';
import type { PropsWithChildren } from 'react';
import { DateTime, Interval } from 'luxon';
import type { DurationUnit, DateInput } from 'luxon';
import { at } from 'lodash';

import './vertical-timeline';
import { toDateTime } from '../common/utils';

interface ITimeLineContext {
  units: DurationUnit;
  unitHeight: number;
  timeLineWidth: number;
  timeLinePadding: number;
  timeLineHeight: number;
  period?: Interval;
  svgWidth: number;
  svgWidthCenter: number;
}

const TimeLineContext = createContext<ITimeLineContext>({
  units: 'years',
  unitHeight: 200,
  timeLineHeight: 0,
  timeLinePadding: 0,
  timeLineWidth: 0,
  svgWidth: 0,
  svgWidthCenter: 0,
});

interface TimeLineItemProps {
  from: DateInput | string | number;
  to?: DateInput | string | number;
}

export const TimeLineItem = ({ from, to = DateTime.now(), children }: PropsWithChildren<TimeLineItemProps>) => {
  return (<TimeLineContext.Consumer>
    {({ timeLinePadding, timeLineHeight, period, units, unitHeight, svgWidth }) => {
      const x = timeLinePadding;
      const y = timeLineHeight - Interval.fromDateTimes(period!.start, toDateTime(to)).length(units) * unitHeight;
      return (<g>
        <TimeLineItemSvgWrapper
          x={x}
          y={y}
          width={svgWidth}
          height={Interval.fromDateTimes(toDateTime(from), toDateTime(to)).length(units) * unitHeight}
        >
          {children}
        </TimeLineItemSvgWrapper>
      </g>);
    }}
  </TimeLineContext.Consumer>);
};

const timeLineItemSvgWrapperDefaultProps: React.SVGProps<SVGForeignObjectElement> = {
  stroke: 'var(--bs-primary)',
  fill: 'none',
  rx: 4,
  ry: 4,
};

const TimeLineItemSvgWrapper = (props: React.SVGProps<SVGForeignObjectElement>) => (
  <foreignObject {...{ ...timeLineItemSvgWrapperDefaultProps, ...props }} />
);

export interface VerticalTimeLineProps {
  from: DateInput | string | number;
  to?: DateInput | string | number;
  units: DurationUnit;
  unitHeight: number;
  width: number;
  timeLinePadding?: number;
}

// const toPeriodReducer = (now: DateInput | string | number): (acc: [DateTime, DateTime], arg: VerticalTimelineItemProps) => [DateTime, DateTime] => (
//   [since, until]: [DateTime, DateTime],
//   { from, to = toDateTime(now) }: VerticalTimelineItemProps,
// ) => {
//   const dtFrom: DateTime = toDateTime(from);
//   const dtTo: DateTime = toDateTime(to);
//
//   if (!since || dtFrom < since) {
//     since = dtFrom;
//   }
//
//   if (dtTo > until!) {
//     until = dtTo;
//   }
//
//   return [since, until];
// };

export const Component = ({ width, unitHeight, units, from, to, children, timeLinePadding = 0 }: PropsWithChildren<VerticalTimeLineProps>) => {
  const now = DateTime.now();
  from = toDateTime(from);
  to = to ? toDateTime(to) : now;
  const period = Interval.fromDateTimes(from, to);
  const height = period.length(units) * unitHeight;
  const timeLineWidth = 14;
  const svgWidthCenter = width / 2;

  return (<svg width={width} height={height} className='vertical-timeline'>
    <rect
      width={timeLineWidth}
      height={height}
      x={svgWidthCenter - timeLineWidth / 2}
      y={0}
      opacity={.2}
      rx={timeLineWidth / 2}
      ry={timeLineWidth / 2}
      className='scale'
    />
    <TimeLineContext.Provider value={{
      units,
      svgWidth: width,
      unitHeight,
      svgWidthCenter,
      timeLinePadding,
      timeLineWidth: 20,
      timeLineHeight: height,
      period,
    }}>
      {children}
    </TimeLineContext.Provider>
  </svg>);
};

export default Component;
