import React from 'react';
import { observer } from 'mobx-react-lite';
import { Container, Image } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';

import type { CvState } from '../mobx';
import ContactsView from './contacts';
import Nav from './navigation';

export interface Props {
  state: CvState;
}

export const Component = observer<Props>(({ state }) => {
  const [t] = useTranslation();

  return (
    <Container fluid className='d-flex flex-column justify-content-center align-items-center h-100'>
      <Image
        roundedCircle
        alt='alt'
        src='/avatar.jpeg'
        className='border-0 p-0 mb-5'
        style={{
          boxShadow: '1px 2px 4px rgba(0, 0, 0, .4), -1px 2px 4px rgba(0, 0, 0, .4)',
          width: '14vh',
          height: '14vh',
        }}
      />
      <h1 className='h5 text-dark fw-bold mb-5' style={{ fontSize: '1.6rem' }}>{t('cv:main_info.value.full_name')}</h1>
      <ContactsView contacts={state.contacts} />
      <Nav />
    </Container>
  );
});

export default Component;
