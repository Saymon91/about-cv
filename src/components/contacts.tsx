import React from 'react';
import { observer } from 'mobx-react-lite';
import { Col, Row } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faFacebook,
  faGithub,
  faGitlab,
  faTelegramPlane,
  faVk,
  faWhatsapp,
} from '@fortawesome/free-brands-svg-icons';
import type { FontAwesomeIconProps } from '@fortawesome/react-fontawesome';

import type { ContactTypes, CvState } from '../mobx';

import './contacts.scss';

export interface Props {
  contacts: CvState['contacts'];
}

interface ContactProps {
  type: ContactTypes;
  link: string;
}

const icons: Partial<Record<ContactTypes, Required<Pick<FontAwesomeIconProps, 'icon' | 'className'>>>> = {
  wa: { icon: faWhatsapp, className: 'whatsapp-icon' },
  tg: { icon: faTelegramPlane, className: 'telegram-icon' },
  fb: { icon: faFacebook, className: 'facebook-icon' },
  vk: { icon: faVk, className: 'vk-icon' },
  gl: { icon: faGitlab, className: 'gitlab-icon' },
  gh: { icon: faGithub, className: 'github-icon' },
}

const ContactView = ({ type, link }: ContactProps) => {
  if (!icons[type]) {
    return null;
  }

  const { icon, className } = icons[type]!;

  return (
    <Col style={{ padding: '0 0.8rem' }}>
      <a href={link} target='blank' className='icon-link'>
        <FontAwesomeIcon icon={icon} className={className} style={{ width: '1rem' }} />
      </a>
    </Col>
  );
};

export const Component = observer<Props>(({ contacts }) => (<Row className='mb-5'>
  {Object.entries(contacts).map(([type, link], index) => (
    <ContactView type={type as ContactTypes} link={link} key={index} />
  ))}
</Row>));

export default Component;
