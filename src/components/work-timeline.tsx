import React, { useEffect, useRef, useState } from 'react';
import { Container } from 'react-bootstrap';
import { DateTime, Interval } from 'luxon';
import { useTranslation } from 'react-i18next';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

import './work-timeline.scss';

import type { Experience } from '../mobx';
import VerticalTimeline, { TimeLineItem } from './vertical-timeline';
import { toDateTime } from '../common/utils';

export type Props = Experience;

interface TimeLineItemDurationProps {
  lineMarkerRadius: number;
}

const TimeLineItemDurationView = styled.div<TimeLineItemDurationProps>`
  width: ${props => props.lineMarkerRadius * 2}px;
  height: ${props => props.lineMarkerRadius * 2}px;
  border-radius: ${props => props.lineMarkerRadius}px;
`;

interface TimeLineDurationProps {
  from: DateTime;
  to: DateTime;
  radius?: number;
}

const TimeLineDuration = ({ from, to, radius = 50 }: TimeLineDurationProps) => {
  const [t, i18n] = useTranslation();

  const fullMonths = Math.ceil(Interval.fromDateTimes(from, to).length('months'));
  const years = Math.floor(fullMonths / 12);
  const months = fullMonths % 12;
  const dec = years ? +years.toString().slice(-2) : 0;
  const toText = to
    ? to.toFormat('LLL yyyy', { locale: i18n.language })
    : t('cv:duration.until_now');
  const fromText = from.toFormat('LLL yyyy', { locale: i18n.language });
  let durationText = '';

  if (years) {
    durationText += t('cv:duration.year_interval', {
      postProcess: 'interval',
      years,
      count: dec >= 20 ? +dec.toString().slice(-1) : dec,
    });
  }
  if (years && months) {
    durationText += ' ';
  }
  if (months) {
    durationText += t('cv:duration.month_short', { months });
  }

  return (
    <TimeLineItemDurationView
      lineMarkerRadius={radius}
      className="d-flex flex-column justify-content-center align-items-center position-relative duration-view"
    >
      <div className="fst-italic">{toText}</div>
      <div className="p-0 m-0 fst-italic fw-bold" style={{ fontSize: '1.2rem' }}>{durationText}</div>
      <div className="fst-italic">{fromText}</div>
    </TimeLineItemDurationView>
  );
};

interface TimeLineItemTitleProps {
  lineMarkerRadius: number;
}

const TimeLineItemTitleView = styled.div<TimeLineItemTitleProps>`
  width: ${props => `calc(100% / 2 - ${props.lineMarkerRadius}px)`};
`;

const timeLineWrapperClasses = ['w-100', 'h-100', 'position-relative', 'item-wrapper', 'd-flex', 'align-items-center', 'justify-content-between'];

type TimelineWrapperProps =
  React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement>
  & { even: boolean };

const TimeLineItemWrapper = ({ className = '', even, ...props }: TimelineWrapperProps) => {
  const classes = [...className!.split(' '), ...timeLineWrapperClasses];
  classes.push(even ? 'flex-row-reverse' : 'flex-row');
  return (<div {...props} className={[...new Set(classes)].join(' ')}/>);
};

export const Component = (state: Props) => {
  const [t, i18n] = useTranslation();
  const [wpSince, wpUntil] = state.workPeriod;
  const containerRef = useRef<HTMLDivElement>(null);
  const [svgWidth, setSvgWidth] = useState(0);
  const durationRadius = 50;

  useEffect(() => {
    if (containerRef.current) {
      // TODO research the best way
      setInterval(() => {
        if (containerRef.current) {
          setSvgWidth(containerRef.current.clientWidth);
        }
      }, 200);
    }
  }, [containerRef]);

  return (
    <Container fluid ref={containerRef}>
      <VerticalTimeline units="years" unitHeight={200} width={svgWidth} from={wpSince} to={wpUntil}>
        {state.jobs.map(({ since, until, position, company, description }, index) => {
          const even = index % 2;

          return (<TimeLineItem from={since} to={until} key={index}>
            <Link
              to={{ pathname: `/experience/${company}/${toDateTime(since).toFormat('yyyy-MM')}${until ? `/${toDateTime(until).toFormat('yyyy-MM')}` : ''}` }}
              style={{ textDecoration: 'none' }}
            >
              <TimeLineItemWrapper even={!!even}>
                <TimeLineItemTitleView lineMarkerRadius={durationRadius}>
                  <p
                    className="p-0 m-0 text-primary"
                    style={{ fontSize: '1.6rem', textAlign: even ? 'right' : 'left' }}
                  >
                    {t(`cv:experience.${position}`)}
                  </p>
                  <p
                    className="p-0 m-0 text-secondary"
                    style={{ fontSize: '1.1rem', textAlign: even ? 'right' : 'left' }}
                  >
                    {t(`cv:experience.${company}`)}
                  </p>
                </TimeLineItemTitleView>
                <TimeLineDuration
                  from={toDateTime(since)}
                  to={until ? toDateTime(until) : DateTime.now()}
                  radius={durationRadius}
                />
                <div style={{ width: `calc(100% / 2 - ${durationRadius}px)` }} className="mh-100 p-2">
                  <p
                    className="text-dark mh-100 overflow-hidden"
                    style={{ textOverflow: 'ellipsis', maxHeight: '100%' }}
                    dangerouslySetInnerHTML={{
                      __html: t(`cv:experience.${description}`, { interpolation: { escapeValue: true } }),
                    }}
                  />
                </div>
              </TimeLineItemWrapper>
            </Link>
          </TimeLineItem>);
        })}
      </VerticalTimeline>
    </Container>
  );
};

export default Component;
