import React from 'react';
import { observer } from 'mobx-react-lite';
import styled from 'styled-components';
import { Table } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';
import { DateTime } from 'luxon';
import Typewriter from 'typewriter-effect';
import { useLocation } from 'react-router-dom';

import type { CvState } from '../mobx';

import './main-info.scss';

export interface Props {
  state: CvState;
}

const ContainerView = styled.main`
  background-image: linear-gradient(to bottom, rgba(0, 0, 0, .8) 0%, rgba(0, 0, 0, .6) 30%, rgba(0, 0, 0, .5) 100%), url("/background.png");
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center center;
  background-blend-mode: normal, screen;
`;

const InfoRow = (
  { children, className = '', ...props }: React.DetailedHTMLProps<React.HTMLAttributes<HTMLTableRowElement>, HTMLTableRowElement>
) => <tr {...props} className={`${className} d-inline-block w-100 pt-3 pb-3`}>{children}</tr>;
const InfoLabelCell = (
  { children, className = '', ...props }: React.DetailedHTMLProps<React.HTMLAttributes<HTMLTableCellElement>, HTMLTableCellElement>
) => <td {...props} className={`${className} fw-bold border-bottom-0 text-capitalize w-50 d-inline-block p-0`}>{children}</td>;
const InfoContentCell = (
  { children, className = '', ...props }: React.DetailedHTMLProps<React.HTMLAttributes<HTMLTableCellElement>, HTMLTableCellElement>
) => <td {...props} className={`${className} border-bottom-0 w-50 d-inline-block p-0`}>{children}</td>;

const phrases = ['name', 'location', 'job'];

export const Component = observer<Props>(({ state }) => {
  const [t, i18n] = useTranslation();
  const location = useLocation();


  const info = [
    [t('cv:main_info.label.name'), t(state.name)],
    [t('cv:main_info.label.age'), state.age],
    [
      t('cv:main_info.label.birth_day'),
      DateTime.fromJSDate(state.birthDay).toLocaleString({}, { locale: i18n.language }),
    ],
    [t('cv:main_info.label.email'), state.email],
    [t('cv:main_info.label.location'), t(state.location)],
  ];

  return (
    <ContainerView
      className="container container-fluid d-flex flex-column m-0 p-0 justify-content-center align-items-center h-100 w-100"
    >
      <div className='pb-5'>
        {location.pathname === '/' && <Typewriter
          options={{
            strings: phrases.map(key => t(`cv:typewriter.row.${key}`)),
            autoStart: true,
            loop: true,
            wrapperClassName: 'display-6 text-light',
            cursorClassName: 'display-6 text-white typewriter-cursor',
          }}
        />}
      </div>
      <Table className="d-block text-white p-0" style={{ width: '40%', fontSize: '1.3em' }}>
        <tbody className='d-block'>
        {info.map(([label, value], index) => (<InfoRow key={index}>
          <InfoLabelCell>{label}:</InfoLabelCell>
          <InfoContentCell>{value}</InfoContentCell>
        </InfoRow>))}
        </tbody>
      </Table>
    </ContainerView>
  );
});

export default Component;
